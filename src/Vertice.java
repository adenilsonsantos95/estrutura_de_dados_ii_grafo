import java.util.*;

public class Vertice {

    private String nome;
    private LinkedList<Vertice> caminhoMaisCurto = new LinkedList<>();
    private Integer pesos = Integer.MAX_VALUE;
    private Map<Vertice, Integer> verticesAdjacentes = new HashMap<>();

    public Vertice(String nome) {
        this.nome = nome;
    }

    public void addDestino(Vertice destino, int pesos) {
        verticesAdjacentes.put(destino, pesos);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Map<Vertice, Integer> getVerticesAdjacentes() {
        return verticesAdjacentes;
    }

    public void setVerticesAdjacentes(Map<Vertice, Integer> verticesAdjacentes) {
        this.verticesAdjacentes = verticesAdjacentes;
    }

    public Integer getPesos() {
        return pesos;
    }

    public void setPesos(Integer pesos) {
        this.pesos = pesos;
    }

    public List<Vertice> getCaminhoMaisCurto() {
        return caminhoMaisCurto;
    }

    public void setCaminhoMaisCurto(LinkedList<Vertice> caminhoMaisCurto) {
        this.caminhoMaisCurto = caminhoMaisCurto;
    }


    @Override
    public String toString() {
        return nome + "(" + pesos + ")" ;
    }
}