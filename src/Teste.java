public class Teste {

    public static void main(String[] args) {
        Vertice verticeA = new Vertice("A");
        Vertice verticeB = new Vertice("B");
        Vertice verticeC = new Vertice("C");
        Vertice verticeD = new Vertice("D");
        Vertice verticeE = new Vertice("E");
        Vertice verticeF = new Vertice("F");
        Vertice verticeG = new Vertice("G");
        Vertice verticeH = new Vertice("H");
        Vertice verticeI = new Vertice("I");
        Vertice verticeJ = new Vertice("J");
        Vertice verticeL = new Vertice("L");

        Grafo grafo = new Grafo();

        grafo.addVertice(verticeA);
        grafo.addVertice(verticeB);
        grafo.addVertice(verticeC);
        grafo.addVertice(verticeD);
        grafo.addVertice(verticeE);
        grafo.addVertice(verticeF);
        grafo.addVertice(verticeG);
        grafo.addVertice(verticeH);
        grafo.addVertice(verticeI);
        grafo.addVertice(verticeJ);

        grafo.addAresta(verticeA, verticeB, 3, 3, 3);
        grafo.addAresta(verticeA, verticeD, 2,3,2);
        grafo.addAresta(verticeD, verticeA, 3,2,2);
        grafo.addAresta(verticeB, verticeC, 4, 3, 3);
        grafo.addAresta(verticeB, verticeE, 2,2,2);
        grafo.addAresta(verticeE, verticeB, 3,2,1);
        grafo.addAresta(verticeC, verticeF, 2, 3, 1);
        grafo.addAresta(verticeF, verticeE, 2,3,2);
        grafo.addAresta(verticeE, verticeF, 3,2,2);
        grafo.addAresta(verticeE, verticeD, 2, 1, 2);
        grafo.addAresta(verticeD, verticeE, 2,1,1);
        grafo.addAresta(verticeD, verticeG, 4,2,3);
        grafo.addAresta(verticeG, verticeH, 1, 4, 1);
        grafo.addAresta(verticeE, verticeH, 3,3,3);
        grafo.addAresta(verticeH, verticeI, 2,2,1);
        grafo.addAresta(verticeF, verticeI, 2, 3, 4);
        grafo.addAresta(verticeI, verticeF, 3,1,4);
        grafo.addAresta(verticeI, verticeJ, 2,1,3);
        grafo.addAresta(verticeJ, verticeI, 3, 1, 2);


        System.out.println(grafo.calcularMenorCaminho(verticeA, verticeG));

    }
}
