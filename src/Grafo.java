import java.util.*;

public class Grafo {

    private Set<Vertice> vertices = new HashSet<>();

    public void addVertice(Vertice vertice) {
        vertices.add(vertice);
    }

    public Set<Vertice> getVertices() {
        return vertices;
    }

    public void setVertices(Set<Vertice> vertices) {
        this.vertices = vertices;
    }

    private boolean checaValidade(Vertice origem, Vertice destino){
        if (!(vertices.contains(origem)) || !(vertices.contains(destino)) || (origem == null) || (destino == null)){
            return false;
        }
        return true;
    }

    public void addAresta(Vertice origem, Vertice destino, int custo, int tempo, int perigo){
        if (this.checaValidade(origem, destino)){
            origem.addDestino(destino, custo+tempo+perigo);
        }
    }

    public  String calcularMenorCaminho(Vertice origem, Vertice destino) {
        if (this.checaValidade(origem, destino)){
            origem.setPesos(0);
            Set<Vertice> verticesVisitados = new HashSet<>();
            Set<Vertice> verticesNaoVisitados = new HashSet<>();
            verticesNaoVisitados.add(origem);
            return this.menorCaminhoRecursivo(origem, destino, verticesVisitados, verticesNaoVisitados);
        }
        return "Existem entradas invalidas";
    }

    private String menorCaminhoRecursivo(Vertice origem, Vertice destino, Set<Vertice> verticesVisitados, Set<Vertice> verticesNaoVisitados){
        if (verticesNaoVisitados.size() != 0) {
            Vertice verticeAtual = this.obterOhMenorPeso(verticesNaoVisitados);
            verticesNaoVisitados.remove(verticeAtual);
            for (Map.Entry<Vertice, Integer> adjacencia : verticeAtual.getVerticesAdjacentes().entrySet()) {
                Vertice verticeAdjacente = adjacencia.getKey();
                Integer pesoAresta = adjacencia.getValue();

                if (!verticesVisitados.contains(verticeAdjacente)) {
                    this.calcularMenorPeso(verticeAdjacente, pesoAresta, verticeAtual);
                    verticesNaoVisitados.add(verticeAdjacente);
                }
            }
            verticesVisitados.add(verticeAtual);
            this.menorCaminhoRecursivo(origem, destino, verticesVisitados, verticesNaoVisitados);
        }
        return this.imprimeMenorPeso(origem, destino);
    }

    private void calcularMenorPeso(Vertice verticeAhAvaliar, Integer pesoAresta, Vertice verticeOrigem) {
        Integer pesoOrigem = verticeOrigem.getPesos();
        if (pesoOrigem + pesoAresta < verticeAhAvaliar.getPesos()) {
            verticeAhAvaliar.setPesos(pesoOrigem + pesoAresta);
            LinkedList<Vertice> caminhoMaisCurto = new LinkedList<>(verticeOrigem.getCaminhoMaisCurto());
            caminhoMaisCurto.add(verticeOrigem);
            verticeAhAvaliar.setCaminhoMaisCurto(caminhoMaisCurto);
        }
    }

    private Vertice obterOhMenorPeso(Set<Vertice> verticeNaoVisitado) {
        Vertice verticeMenorPeso = null;
        int menorPeso = Integer.MAX_VALUE;
        for (Vertice vertice : verticeNaoVisitado) {
            int pesoVertice = vertice.getPesos();
            if (pesoVertice < menorPeso) {
                menorPeso = pesoVertice;
                verticeMenorPeso = vertice;
            }
        }
        return verticeMenorPeso;
    }

    public String imprimeMenorPeso(Vertice origem, Vertice destino){
        String d = "Menor Caminho de " + origem.getNome() + " ~ " + destino.getNome() + ": ";
        if (this.checaValidade(origem, destino)){
            for (int i = 0; i < destino.getCaminhoMaisCurto().size(); i++) {
                d = d + destino.getCaminhoMaisCurto().get(i).getNome() + " -> ";
            }
            d = d + destino.getNome() + " - Distancia: (" + destino.getPesos() + ")";
            if (destino.getPesos() == Integer.MAX_VALUE){
                return ("Não existe caminho de :" + origem.getNome() + " ~ " + destino.getNome());
            }
        }
        return d;
    }
}
